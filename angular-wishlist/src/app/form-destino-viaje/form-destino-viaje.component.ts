import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.models';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>
fg: FormGroup;
minimo = 5;

  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['',Validators.compose([
        Validators.required,
        this.nombreValidator
      ]) ],
      url: ['']
    }); /* las variables de los formularios, como agregarlas, los formconrols de los tags*/
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario: ', form);
    }
    )
  }

  ngOnInit(): void {
  }

  guardar(nombre: string, url:string): boolean {
    let d = new DestinoViaje(nombre,url);
    this.onItemAdded.emit(d);
    return false;
  }

    /*'required':true */
  nombreValidator(control: FormControl): {[s:string]:boolean} | null  {
    let l = control.value.toString().trim.length;
    if (l > 0 && l < 5 ) {
      return {invalidNombre: true};
    }
    return  null;
  }
  


}
