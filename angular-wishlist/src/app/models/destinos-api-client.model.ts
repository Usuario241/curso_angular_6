import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from '../models/destino-viaje.models';

export class DestinosApiClient {

  destinos: DestinoViaje[];
  current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(new DestinoViaje("",""));
  constructor() {
    this.destinos = [];
  }

  add(d: DestinoViaje) {
    this.destinos.push(d);
  }
  getAll(): DestinoViaje[] {
    return this.destinos;
  }
  getById(id: string): DestinoViaje {
    return this.destinos.filter(function (d) { return d.toString() === id; })[0];
  }

  elegir(d: DestinoViaje) {
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);
  }

  subscribeOnChance(fn: any){
    this.current.subscribe(fn);
  }

}