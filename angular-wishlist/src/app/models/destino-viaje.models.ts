    export class DestinoViaje{
        nombre:string;
        imagenUrl:string;

        public servicios: string[];
        private selected: boolean ;

        constructor( n :string, u:string){ 
            this.nombre=n;
            this.imagenUrl=u;
            this.selected = false;
            this.servicios = ['Piscina','Desayuno','Parqueo'];
         }
         /*
         constructor (public nombre: string, public u:string){}
         */
        isSelected(): boolean{
            return this.selected;
        }
        
        setSelected(s: boolean){
            this.selected=s;
        }
    }
