import { ThrowStmt } from '@angular/compiler';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.models';
import { DestinosApiClient } from '../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>;
updates: string[];

public destinosApliClient: DestinosApiClient

  constructor() {
    this.onItemAdded = new EventEmitter();
    this.destinosApliClient = new DestinosApiClient();
    this.updates = [];
    this.destinosApliClient.subscribeOnChance((d: DestinoViaje) => {
      if (d != null) {
        this.updates.push('Se ha elegido a ' + d.nombre);
      }
    });
   }

  ngOnInit(): void {
  }

  
  agregado(d: DestinoViaje) {
   this.destinosApliClient.add(d);
   this.onItemAdded.emit(d);
  }

  elegido(e: DestinoViaje) {
    this.destinosApliClient.elegir(e);    
  }

}
